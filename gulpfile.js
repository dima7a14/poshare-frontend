'ust strict';

const gulp = require('gulp');
const sass = require('gulp-ruby-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const minifycss = require('gulp-minify-css');
const sourcemaps = require('gulp-sourcemaps');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const notify = require('gulp-notify');
const cache = require('gulp-cache');
const del = require('del');
const gulpIf = require('gulp-if');
const browserSync = require('browser-sync').create();

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';


gulp.task('styles', function() {
  return sass('css/main.scss', { style: 'expanded', 'sourcemap': true })
    .pipe(postcss([ autoprefixer({ browsers: ['> 0%'] }) ]))
    .pipe(gulpIf(isDev, sourcemaps.write()))
    .pipe(gulp.dest('css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css'));
    // .pipe(notify({ message: 'Styles task complete!' }));
});

gulp.task('styles:homepage', function() {
  return sass('css/homepage.scss', { style: 'expanded', 'sourcemap': true })
    .pipe(postcss([ autoprefixer({ browsers: ['> 0%'] }) ]))
    .pipe(gulpIf(isDev, sourcemaps.write()))
    .pipe(gulp.dest('css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css'));
    // .pipe(notify({ message: 'Homepage styles task complete!' }));
});

gulp.task('scripts', function() {
  return gulp.src('js/app.js')
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
    // .pipe(notify({ message: 'Scripts task complete' }));
});

// gulp.task('images', function() {
//   return gulp.src('img/**/*')
//     .pipe(newer('img'))
//     .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//     .pipe(gulp.dest('img'))
//     .pipe(notify({ message: 'Images task complete' }));
// });


gulp.task('build', gulp.series(
  // 'clean',
  gulp.parallel('styles', 'styles:homepage', 'scripts')
));

gulp.task('watch', function() {
  
  gulp.watch('css/**/*.scss', gulp.parallel('styles', 'styles:homepage'));
  gulp.watch('js/app.js', gulp.series('scripts'));

});

gulp.task('serve', function() {
  browserSync.init({
    server: './'
  });

  // Watch any files in dist/, reload on change
  browserSync.watch(['css/main.css', 'css/homepage.css', 'js/*.js', './*.html']).on('change', browserSync.reload);
});


gulp.task('dev',
  gulp.series('build', gulp.parallel('watch', 'serve'))
  // gulp.start('serve');
);
