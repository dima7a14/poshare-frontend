
function oauthpopup(options) {
	options.windowName = options.windowName || 'ConnectWithOAuth';
	options.windowOptions = options.windowOptions || 'location=0,status=0,width='+options.width+',height='+options.height+',scrollbars=1';
	options.callback = options.callback || function () {
		window.location.reload();
	};
	var that = this;
	that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
	console.log(that._oauthWindow);
	that._oauthInterval = window.setInterval(function () {
		if (that._oauthWindow.closed) {
			window.clearInterval(that._oauthInterval);
			options.callback();
		}
		}, 1000);
};

$('#fblogin').click(function(e){
			oauthpopup({
				path: 'http://poshare.com/index.php?route=account/login/with_facebook/',
				width:800,
				height:400,
				callback: function(){
					window.location.reload();
				}
			});
			e.preventDefault();
});

$('#fbsignup').click(function(e){
			var reg = $('#individual').is(':checked')?'individual':'business';
			oauthpopup({
				path: 'http://poshare.com/index.php?route=account/register/with_facebook&reg=' + reg,
				width:800,
				height:400,
				callback: function(){
					window.location.reload();//window.location.href='http://poshare.com/index.php?route=account/profile';
				}
			});
			e.preventDefault();
});