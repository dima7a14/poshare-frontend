$(document).ready(function() {
var poshareApp = (function () {
  var _this;
  return _this = {
  clndr: {
    getData: function () {
      var data = $.get('./orders.json');
      return data;
      return $.ajax({
        url: './orders.php',
        type: 'POST',
        dataType: "json"
      });
    },
  },
  validator: {
    form: null,
    buttonArr: null,
    settings: {
      errorElement: 'div',
      submitHandler: function(form) {
        form.submit();
      },
      rules: {
        fname: {
          required: true,
          rangelength: [1, 15],
          letterswithbasicpunc: true
        },
        lname: {
          required: true,
          rangelength: [1, 15],
          letterswithbasicpunc: true
        },
        name: {
          required: true,
          rangelength: [1, 15],
          letterswithbasicpunc: true
        },
        password: {
          required: true,
          minlength: 8
        }
      },
      messages: {
        zip: {
          zipcodeUS: "Invalid ZIP"
        },
        fname: "Invalid first name",
        lname: "Invalid last name",
        name: "Invalid name",
        password: "Please enter at least 8 symbols"
      },
      onfocusout: function (el, e) {
        $(el).valid();
        _this.validator.btnCondition();
      },
      onkeyup: function(el, e) {
        _this.validator.btnCondition();
        _this.validator.remoteZipValid(el);
      },
      errorPlacement: function(label, element){
        label.insertAfter(element);
        element.focus(function(){
        $(this).removeClass('error').next('div.error').hide();
        });
      }
    },
    init: function (form) {
      _this.validator.form = form;
      _this.validator.form.on('change', function(e) {
        _this.validator.btnCondition();
      });
      /*$.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value);
      }, "Invalid email");*/
    },
    btnCondition: function () {
      var valid,
        form = _this.validator.form,
        button = this.buttonArr || $("[type=submit]", form);
      //
      valid = form.validate().checkForm();
      //
      $.each(button, function(index, el) {
        $(el).prop("disabled", true);
        if (valid) {
          $(el).prop("disabled", false);
        };
      });
      return valid;
    },
    remoteZipInit: function (form) {
      var zip = $("input[data-name='zip']", form),
        select = $("select[data-name='zip-addr']", form);
      // show/hide zip-addr select
      zip.each(function (i, e) {
        if ($(e).val()) {
          $(select[i]).show();
        };
      });
      //
      form.on("change", "select[data-name='zip-addr']", function(e) {
        var def_box = _this.validator.getZipBox(e.target, "city-state-box"),
          val = $(e.target).val();
        //
        if (val === 'Other') {
          def_box.show();
          _this.validator.btnCondition();
        } else {
          def_box.hide();
        };
      });
    },
    remoteZipValid: function (el) {
      if ($(el).data("name") != "zip") {
        return;
      };
      var data = {},
        newOpt = '',
        cityBox = _this.validator.getZipBox($(el), "city-state-box"),
        zipBox = _this.validator.getZipBox($(el), "zip-addr-box");
      //

      if ( $(el).val() && _this.validator.form.validate().check(el) ) {
        $.ajax({
          url: "/index.php?route=account/profile/zip_func",
          type: "POST",
          data: {"zip": $(el).val()},
          dataType: "JSON",
          success: function(res) {
            if ($.isPlainObject(res.data)) {
              data[res.data.State] = res.data.City+", "+res.data.State;
              zipBox.find('option').remove();
              zipBox.show();
              cityBox.hide();
            } else {
              cityBox.show();
              zipBox.hide();
              _this.validator.btnCondition();
            };
            // add response data to select
            data.other = "Other";
            $.each(data, function(key, val) {
              newOpt += '<option value="'+ val +'">'+ val +'</option>';
            });
            zipBox.html(newOpt);
          }
        });
      } else {
        cityBox.hide();
        zipBox.hide();
      };
    },
    getZipBox: function (el, p) {
      // el - is input[name=zip]
      if (p == "city-state-box") {
        // return div.city-state-box
        return $(el).parents('div.form_element').next('div.city-state-box');
      } else if (p == "zip-addr-box") {
        // return input[name=zip-addr]
        return $(el).parent('div').next('div.zip-addr-box').children('select');
      };
    }
  },
  user_dropdown: function () { //***top menu user dropdown***
    var menu = $('.top-menu'),
      user = $('li.user', menu),
      dropdown = $('.user__dropdown', menu);
    //
    if (!user.length) {
      return;
    };
    user.click(function() {
      if ( ! dropdown.is(":visible") ) {
        $(this).addClass('arrow-down');
        dropdown.show();
        return;
      }
      hideMenu($(this));
    });
    $(window).on("click touchstart", hideMenu);
    //
    function hideMenu (el) {
      if ( ! $.contains(user[0], el.target) ) {
        user.removeClass('arrow-down');
        dropdown.hide();
      };
    }
  },
};
}());
//
poshareApp.user_dropdown();


// manage calendar
  (function(app) {
  var _this;
  return _this = {
    clndrBox: $('#calendar-box'),
    clndrTmpl: null,
    clndrInst: null,
    infoBox: $("#info-calendar-box"),
    infoTmpl: $("#info-calendar"),
    timeFormat: 'YYYY-MM-DD',
    data: null,
    allOrders: [],
    newEvent: [],
    hrData: {
      month: null,
      start: null,
      startIndex: null,
      lastIndex: null,
      days: null
    },
    size: $("#sizes").val(),
    init: function(tpl) {
      // check template to init script
      if (! tpl.length)
        return;
      //
      app.clndr.getData()
      .done(function (res) {
        _this.data = res.data;
        _this.allOrders = _this.data.orders;
        _this.filterOrdersBySize();
        _this.clndrTmpl = _.template(tpl.html());
        _this.addHandlers();
        _this.toggleClndr();
        _this.clndrInst = _this.clndrBox.clndr({
          render: function(data) {
            return _this.clndrTmpl(data);
          },
          events: _this.data.orders,
          multiDayEvents: {
            startDate: 'startDate',
            endDate: 'endDate',
            singleDay: "date",
          },
          ready: function() {},
          showAdjacentMonths: false,
          adjacentDaysChangeMonth: true,
          forceSixRows: false,
          clickEvents: {
            click: function(el) {
              _this.addNewRange(el);
            },
            nextMonth: function () {
              var start = _this.hrData.start,
                i = 0,
                clndrReg = /calendar-day-(\d{4})-(\d{2})-(\d{2})/,
                startClass;
              _this.hrData.days = $('td.day', _this.clndrBox);
              if (start) {
                startClass = start[0].className.match(clndrReg)[0];
                if (startClass) {
                  _.each(_this.hrData.days, function (el, index) {
                    if ($(el).hasClass(startClass)) {
                      i = index;
                      $(el).addClass('hovered');
                    }
                  });
                  _this.hrData.startIndex = _this.hrData.lastIndex = i;
                };
              };
              _this.renderInfoBox();
              // _this.hrData.days = $('td.day', _this.clndrBox);
            },
            previousMonth: function(date){
              var firstDate = _this.hrData.firstDate,
                month = date.month(),
                needClass;
              // start hovered on prev month
              if (month == _this.hrData.month && _this.hrData.start) {
                firstDate.removeClass("hovered")
                needClass = firstDate[0].className;
                _this.hrData.days = $('td.day', _this.clndrBox);
                if (needClass) {
                  _.each(_this.hrData.days, function (el, index) {
                    if ( $(el)[0].className == needClass ) {
                      _this.hrData.startIndex = _this.hrData.lastIndex = index;
                      $(el).addClass('hovered');
                    }
                  });
                };
              };
              _this.renderInfoBox();
            }
          }
        });
        _this.renderInfoBox();
      });
    },
    toggleClndr: function() {
      var size = this.size;
      if (!size) {
        this.infoBox.fadeOut(0);
        this.clndrBox.fadeOut(0);
      } else {
        this.infoBox.slideDown();
        this.clndrBox.slideDown();
      }
    },
    filterOrdersBySize: function() {
      _this.data.orders = _this.allOrders.filter(function(order) {
        if (order.size) {
          return _this.size === order.size;
        }
        return false;
      });
    },
    addNewRange: function (el) {
      var format = _this.timeFormat,
        newEvent = _this.newEvent,
        pickDate = el.date.format(format),
        nowDate = moment().format(format),
        isBefore = el.date.isBefore(nowDate);
      // return if click on past date or event
      // isBefore
      if ($(el.element).hasClass('event') || isBefore) {
        // reset newEvent array
        newEvent.length = 0;
        _this.hoverRange($(el.element), 'end');
        return;
      };
      //
      if (_.isEmpty(newEvent)) {
        newEvent[0] = {};
        newEvent[0].startDate = pickDate;
        // add hover range highlight
        _this.hoverRange($(el.element), 'start');
      } else {
        if (_.size(newEvent[0]) == 1) {
          // prevent left to right date pick
          if ( moment(pickDate).isBefore(moment(newEvent[0].startDate)) ) {
            return;
          };
          //
          newEvent[0].endDate = pickDate;
          if ( ! _this.isOverlapsRange(newEvent[0]) ) {
            _this.addReserv(newEvent[0]);
          };
          // reset newEvent array
          newEvent.length = 0;
          _this.hoverRange($(el.element), 'end');
        };
      };
    },
    hoverRange: function (el, evt) {
      var start, days, startIndex, elIndex, lastIndex;
      if (evt == "start") {
        // init obj
        _this.hrData.month = _this.clndrInst.month.month();
        _this.hrData.start = _this.hrData.firstDate = el;
        _this.hrData.days = $('td.day', _this.clndrBox);
        _this.hrData.startIndex = _this.hrData.lastIndex = _this.getHoverRangeIndex(el);
        el.addClass('hovered');
      } else if (_this.hrData.start && evt != "end") {
        days = _this.hrData.days;
        startIndex = _this.hrData.startIndex;
        lastIndex = _this.hrData.lastIndex;
        elIndex = _this.getHoverRangeIndex(el);
        //
        if (evt == "mouseenter" && elIndex >= startIndex) {
          if (elIndex > lastIndex ) {
            days.slice(startIndex, elIndex + 1).addClass('hovered');
          } else if (elIndex <= lastIndex) {
            days.slice(elIndex + 1, lastIndex + 1).removeClass('hovered');
          }
          _this.hrData.lastIndex = elIndex;
        }
      } else if (evt == "end") {
        _this.hrData.days = $('td.day', _this.clndrBox);
        _this.hrData.days.removeClass("hovered");
        _this.hrData.start = null;
      }
    },
    getHoverRangeIndex: function (el) {
      var days = _this.hrData.days,
        r = false;
      days.each(function (i, day) {
        if (day == el[0])
          r = i;
      });
      return r;
    },
    renderInfoBox: function () {
      var compiled = _.template(this.infoTmpl.html()),
        endMonth, startMonth, monthRange, index = [];
      this.infoTmpl.remove();
      this.infoBox.empty();

      if (this.clndrInst) {
        startMonth = this.clndrInst.month;
        endMonth = moment(this.clndrInst.month).endOf("month"); // clone curent date
        monthRange  = moment().range(startMonth, endMonth);
        _.each(this.data.orders, function (event, i) {
          // console.log(event.startDate, event.endDate);
          // console.log(moment(event.startDate).within(monthRange), moment(event.endDate).within(monthRange));
          if ( !moment(event.startDate).within(monthRange) && !moment(event.endDate).within(monthRange) ) {
            index.push(i);
          };
        });
      };
      this.infoBox.append( compiled(this.data) );
      _.each(index, function (val) {
        $('li[data-id='+ val +']', _this.infoBox).hide();
      });
      $("textarea", _this.infoBox).textareaAutoSize();
      $("button.save", _this.infoBox).hide();
    },
    isOverlapsRange: function (event) {
      var events = _this.data,
        newRange,
        isOverlaps = false,
        curentRange = moment().range(event.startDate, event.endDate);
      //
      _.each(events, function (arr, key) {
        _.each(arr, function (obj, key) {
          newRange = moment().range(obj.startDate, obj.endDate);
          if (newRange.overlaps(curentRange)) {
            isOverlaps = true;
          };
        });
      });
      return isOverlaps;
    },
    addReserv: function(data) {
      var clndr = _this.clndrInst,
        orders = _this.data.orders;
      //
      // if ( !this.validateDedcrBox($('textarea', this.infoBox).first()) )
        // return;
      data.description = "";
      data.reserved = false;
      data.size = this.size;
      orders.push(data);
      clndr.setEvents(orders);
      _this.renderInfoBox();
      $('textarea', this.infoBox).first().addClass("editable").prop("disabled", false).focus()
        .siblings('button.edit').hide()
        .siblings('button.save').show();
    },
    validateDedcrBox: function (descrBox) {
      if (descrBox.val() === "") {
        descrBox.addClass("error").prop("disabled", false).focus();
        return false;
      } else {
        descrBox.removeClass("error").focus();
      };
      return true;
    },
    removeReserv: function (id) {
      var eventRange,
        orders = _this.data.orders,
        rmEvent = orders[id],
        rmEventRange = moment().range(rmEvent.startDate, rmEvent.endDate);
      //
      _this.clndrInst.removeEvents(function (event) {
        eventRange = moment().range(event.startDate, event.endDate);
        // compare events
        if ( eventRange.isSame(rmEventRange) ) {
          return true;
        };
      });
      orders.splice(id, 1);
      _this.renderInfoBox();
    },
    addHandlers: function () {
      $("#sizes").on("change", function() {
        var newSize = $(this).val();
        if (newSize !== _this.size) {
          _this.size = newSize;
          _this.filterOrdersBySize()
          _this.clndrInst.setEvents(_this.data.orders);
          _this.renderInfoBox();
        }
        _this.toggleClndr();
      });
      _this.infoBox.on("click", "button.delete", function (e) {
        _this.removeReserv( $(e.target).data("id") );
      });
      _this.infoBox.on("click", "button.save", function (e) {
        var el = $(e.currentTarget),
          textarea = el.siblings('textarea'),
          edit = el.siblings('button.edit'),
          id = el.data("id");
        // return if invalid description
        if ( !_this.validateDedcrBox(textarea) ) {
          return;
        };
        _this.data.orders[id].description = textarea.val();
        textarea.removeClass("editable").prop('disabled', true).removeClass("error");
        edit.show();
        el.hide();
      });
      _this.infoBox.on("click", "button.edit", function (e) {
        var el = $(e.currentTarget),
          save = el.siblings('button.save'),
          textarea = el.siblings('textarea');
        //
        textarea.addClass("editable").prop('disabled', false).focus();
        el.hide();
        save.show();
      });
      _this.clndrBox.on('mouseenter mouseleave', 'td.day',  function (e) {
        var el = $(e.currentTarget),
          event = e.type;
        //
        _this.hoverRange(el, event);
      });
      _this.clndrBox.on("click", "span.clndr-next-button, span.clndr-previous-button", function (e) {
        if ( ! _this.validateDedcrBox($('textarea', this.infoBox).first()) ) {
          e.stopImmediatePropagation();
          return false;
        };
      });
    }
  }
})(poshareApp).init($('#template-calendar'));

// item order calendar
(function(app) {
  var _this;
  return _this = {
    clndrTmpl: null,
    clndrInst: null,
    clndrBox: $('#calendar-box'),
    timeFormat: 'YYYY-MM-DD',
    form: $('#rent-form'),
    dateRange: [],
    data: null,
    allOrders: [],
    size: $("#sizes").find(":checked").text(),
    init: function(tpl) {
      // check template to init script
      if (! tpl.length)
        return;
      app.clndr.getData().
      done(function (res) {
        _this.data = res.data;
        _this.allOrders = _this.data.orders;
        _this.filterOrdersBySize();
        _this.removeOrders();
        _this.clndrTmpl = _.template(tpl.html());
        _this.clndrInst = _this.clndrBox.clndr({
          events: _this.data.orders,
          showAdjacentMonths: false,
          adjacentDaysChangeMonth: true,
          forceSixRows: false,
          multiDayEvents: {
            startDate: 'startDate',
            endDate: 'endDate',
            singleDay: "date",
          },
          clickEvents: {
            click: function (el) {
              if ( ! $(el.element).hasClass("day") || $(el.element).hasClass("weekend") )
                return;
              var newOrder = _this.checkReserv(el);
              //
              // remove previous reservation
              _this.changeReserv("remove", null, "keep");
              //
              if ( newOrder ) {
                // add new order
                setTimeout((function (newOrder) {
                  return function () {
                    _this.changeReserv("add", newOrder);
                    $('.item__add').slideDown(200);
                  }
                })(newOrder), 10);
              } else {
                $('.item__add').slideUp(200);
              }
            }
          },
          render: function(data) {
            data.size = _this.size;
            return _this.clndrTmpl(data);
          }
        });
        _this.addHandlers();
      });
    },
    filterOrdersBySize: function() {
      _this.data.orders = _this.allOrders.filter(function(order) {
        if (order.size) {
          return _this.size === order.size;
        }
        return true;
      });
    },
    checkReserv: function(el) {
      var keep = _this.getOps("keep"),
        startMoment = el.date,
        isAfter = (el.date) ? startMoment.isAfter(moment()) : false,
        endMoment,
        size = _this.size,
        newOrder = {};
      //
      if (keep !== false && isAfter) {
        endMoment = moment(startMoment).add(keep - 1, "days");
        newOrder.startDate = startMoment.format(_this.timeFormat);
        newOrder.endDate = endMoment.format(_this.timeFormat);
        newOrder.arrive = true;
        newOrder.size = size;
        //
        if ( ! _this.isOccupiedRange(startMoment, endMoment) )
          return newOrder;
      };
      return false;
    },
    // action: remove, add;
    // data: event obj for clndr;
    changeReserv: function(action, data, type) {
      var clndr = _this.clndrInst;
      //
      if (action == "remove") {
        if (type == "keep") {
          _this.spliceOrders("arrive");
          if ($("input[name=keep]:checked").val() != 1) {
            _this.spliceOrders("forever");
          };

        } else if (type == "delivery") {
          _this.spliceOrders("delivery");
        };
      } else if (action == "add") {
        if (type == "delivery") {
          data.delivery = true;
          _this.data.orders.push(data);
        } else {
          data.reserved = true;
          _this.data.orders.push(data);
          _this.changeDateRange(_this.data.orders);
        }
      };
      clndr.setEvents(_this.data.orders);
    },
    changeDateRange: function(orders) {
      var orders = orders || [];
      if ( $('.date-range').length ) {
        $('.date-range').remove();
      }
      orders.forEach(function(el) {
        if (el.reserved) {
          var dateRangeEl = document.createElement('div');
          dateRangeEl.setAttribute('class', 'date-range');
          dateRangeEl.textContent = changeDateFormat(el.startDate) + ' - ' + changeDateFormat(el.endDate);
          document.getElementById('calendar-box').appendChild(dateRangeEl);
        }
      }, _this);
      function changeDateFormat(date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var year = date.substring(0, date.indexOf('-'));
        date = date.slice(date.indexOf('-') + 1);
        var month = months[ +date.substring(0, date.indexOf('-')) - 1 ];
        date = date.slice(date.indexOf('-') + 1);
        var day = +date;
        return month + ' ' + day + ' ' + year;
      }
    },
    spliceOrders: function (key) {
      var orders = _this.data.orders,
        newArr = [];
      //
      for (var i = 0; i < orders.length; i++) {
        if (!orders[i][key])
          newArr.push(orders[i]);
      };
      //
      _this.data.orders = newArr;
    },
    isOccupiedRange: function (start, end) {
      var newRange,
        events = _this.clndrInst.options.events,
        isOccupied = false,
        curentRange = moment().range(start, end);
      //
      for (var i = 0; i < events.length; i++) {
        var startDate = events[i]._clndrStartDateObject,
          endDate = events[i]._clndrEndDateObject;
        //
        if ( curentRange.contains(startDate) || curentRange.contains(endDate) ) {
          isOccupied = true;
        };
      };
      return isOccupied;
    },
    foreverKeep: function () {
      var events = _this.clndrInst.options.events,
        nowDate = moment().format(_this.timeFormat),
        maxDate;
      //
      for (var i = 0; i < events.length; i++) {
        var endDate = events[i]._clndrEndDateObject;
        //
        maxDate = moment.max(maxDate, endDate);
      };
      _this.changeReserv("add", {startDate: nowDate, endDate: maxDate.format(_this.timeFormat), forever: true});
    },
    addDelivery: function (val) {
      var events = _this.clndrInst.options.events,
        nowDate = endDate = moment().format(_this.timeFormat);
      //
      if (val == "shipping") {
        endDate = moment().add(4, "days").format(_this.timeFormat);
      };
      _this.changeReserv("add", {startDate: nowDate, endDate: endDate, delivery: true}, "delivery");
    },
    // get delivery or keep radio button value
    getOps: function (op) {
      var el = $("input[name="+ op +"]:checked", _this.form);
      //
      return el.length ? el.val() : false;
    },
    addHandlers: function () {
      _this.form.on("change", "input[name=keep], input[name=delivery]", function () {
        var el = $(this);
        _this.changeReserv("remove", null, el.attr("name"));
        if (el.attr("name") === "keep") {
          _this.form.find(".item-option__delivery-methods").slideDown(200);
          if (_this.form.find("input[name=delivery]:checked").prop("checked")) {
            if (el.hasClass("sale_check")) {
              _this.form.find(".item-clndr__info li:last-child").hide();
            } else {
              _this.form.find(".item-clndr__info li:last-child").show();
            }
            _this.form.find(".item-clndr").slideDown(200);
          }
          if (el.val() === 1) {
            _this.foreverKeep();
          }
        };
        if (el.attr("name") === "delivery") {
          _this.form.find(".item-clndr").slideDown(200);
          _this.addDelivery(el.val());
        }
      });
      
      _this.form.ready(function(e) {
        var keepTrigger = _this.form.find("input[name=keep]:checked").prop("checked"),
            deliveryTrigger = _this.form.find("input[name=delivery]:checked").prop("checked");
        if (keepTrigger) {
          _this.form.find(".item-option__delivery-methods").slideDown(200);
        }
        if (keepTrigger && deliveryTrigger) {
          if (_this.form.find("input[name=keep]:checked").hasClass("sale_check")) {
            _this.form.find(".item-clndr__info li:last-child").hide();
          } else {
            _this.form.find(".item-clndr__info li:last-child").show();
          }
          _this.form.find(".item-clndr").slideDown(200);
        }
        if (deliveryTrigger) {
          _this.addDelivery(_this.form.find("input[name=delivery]:checked").val());
        }
      });

      _this.form.find("#sizes").on("change", function(e) {
        var newSize = $(this).find(":checked").text();
        if (newSize !== _this.size) {
          _this.size = newSize;
          _this.filterOrdersBySize();
          _this.clndrInst.setEvents(_this.data.orders);
          _this.addDelivery($('input[name="delivery"]').filter(':checked').val());
        }
      });
    },
    removeOrders: function () {
      var cloneEvents = [];
      //
      $.each(this.data.orders, function(index, event) {
        if (event.ordered) {
          cloneEvents.push(event);
        };
      });
      this.data.orders = cloneEvents;
    }
  }
})(poshareApp).init($('#template-calendar-item'));


  (function search_click() {
    if ($(window).width() >= 720) {
      var $wr = $('.block-search'),
        $input = $wr.find('.input-search input');
      $wr.find('.show-search').on('click', function() {
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
        $(this).fadeOut(0);
        $wr.parent().find('.main-nav').fadeOut(0);
        $wr.addClass('active');
        $input.focus();
        closeonclickoutside($wr, $wr.find('button'), function() {
          $('.show-search').fadeIn(0);
          $wr.parent().find('.main-nav').fadeIn(0);
          $wr.removeClass('active');
        });
      });
    } else {
      var $wr = $('.top-menu__search-icon'),
        $input = $wr.find('.input-search input');

      $wr.on('click', function() {
        $wr.addClass('active');
        $input.focus();
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
        closeonclickoutside($wr, $wr.find('button'), function() {
          $('.show-search').fadeIn(0);
          $wr.removeClass('active');
        });
      });
    }
    var ww = $(window).width();
    $(window).resize(function() {
      if (ww < 720) {
        if ($(window).width() >= 720) {
          ww = $(window).width();
          search_click();
        }
        ww = $(window).width();
      } else {
        if ($(window).width() < 720) {
          ww = $(window).width();
          search_click();
        }
      }
    });
  })();


  // (function menu_width_calc(drop) {
  //   if ($(window).width() >= 720) {
  //     var dropWidth = drop.width();
  //     var subMenuCounts = drop.find('ul').length;
  //     drop.has('ul').css('width', dropWidth * subMenuCounts + 'px');
  //     drop.find('ul').css('width', 100 / subMenuCounts + '%');
  //   }

  //   var ww = $(window).width();
  //   $(window).resize(function() {
  //     if (ww < 720) {
  //       if ($(window).width() >= 720) {
  //         ww = $(window).width();
  //         var dropWidth = drop.width();
  //         var subMenuCounts = drop.find('ul').length;
  //         drop.has('ul').css('width', dropWidth * subMenuCounts + 'px');
  //         drop.find('ul').css('width', 100 / subMenuCounts + '%');
  //       }
  //     } else {
  //       if ($(window).width() < 720) {
  //         ww = $(window).width();
  //         drop.has('ul').css('width', 'auto');
  //         drop.find('ul').css('width', 'auto');
  //       }
  //     }
  //   });
  // })($('.menu__dropdown'));




  function menu_click_hover(list) { //change menu interaction from hover to click and vice versa
    if ($(window).width()>=720) {
      // $('.menu__dropdown').css('display','none'); //hide the dropdown
      // $('.menu__nav .horizontal-list--inline li').removeClass('open').find('span').unbind().parent() //remove class used for onclick event, unbind those events
      //  .mouseenter(function(){ //onmouseenter show block, else - hide block
      //    $(this).find('.menu__dropdown').css('display','block');
      //  })
      //  .mouseleave(function(){
      //    $(this).find('.menu__dropdown').css('display','none');
      //  });
    }
    else {$(list).children('li').unbind().children('span').unbind() //unbind events from an element
       .bind("click", function(event) {
          if ($(this).parent().children('ul').css('display') == 'none') { //if element is hidden
            $(this).parent().children('ul').css('display','none'); //hide other dropdowns from the menu
            $(list).children('li').removeClass('open'); //remove class added to previously mentioned elements
            $(this).parent().children('ul').css('display', 'block'); //show dropdown
            $(this).parent().addClass('open'); //change it's look
          } else {
            $(this).parent().children('ul').css('display', 'none'); //if it is open - onclick hide it
            $(this).parent().removeClass('open'); //and change its look to default one
          }
        });
      $(list).children('li').children('.menu__dropdown').css('display','none'); //hide the dropdown
    }
  }


  function page_menu_scroll () {
    var active_element_position = $('.page-menu').find('.active').position().left;
    var active_element_width = $('.page-menu').find('.active').outerWidth(true);

    if ((active_element_position + active_element_width) > $(window).width()) {
      $('div.page-menu').scrollLeft(active_element_position+active_element_width-$(window).width()+(($(window).width()-active_element_width)/2));
    }
  }


  function img_slider() {

    var thumbid=[], thumbidradio=[], slideid=[], thumbid_temp=0;

    $('.image-slider__thumb').find('img').each(function(){
      $(this).attr("id","t"+thumbid_temp);
      thumbid[thumbid_temp]="t"+thumbid_temp;
      ++thumbid_temp;
    }
    );
    thumbid_temp=0;

     $('.image-slider__thumb').find('input').each(function(){
      $(this).attr("id","c"+thumbid_temp);
      thumbidradio[thumbid_temp]="c"+thumbid_temp;
      ++thumbid_temp;
    }
    );

     thumbid_temp=0;

    $('.image-slider').find('img').each(function(){
      $(this).attr("id","img"+thumbid_temp);
      slideid[thumbid_temp]="img"+thumbid_temp;
      ++thumbid_temp;
      }
    );

    var selecteditem = 0;

    $('.image-slider__thumb').find('img').click(function() {

      selecteditem = thumbid.indexOf($(this).attr('id'));

      $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});

      $('.image-slider__thumb').find('input').prop('checked', false);
      $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent',
      'cursor':'pointer'})

      $(this).css({'border' : '2px solid #826072'});
      $('#'+thumbidradio[selecteditem]).prop('checked', true);


    });

    $('.image-slider__thumb').find('input').click(function() {
      selecteditem = thumbidradio.indexOf($(this).attr('id'));

      $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});

      $('.image-slider__thumb').find('input').prop('checked', false);
      $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent'})


      $(this).prop('checked', true);
      $('#'+thumbid[selecteditem]).css({'border' : '2px solid #826072'});

    });

    var desktopswipe=false;

    jQuery('.image-slider')
        .on('swipeleft', function(e) {
          if (desktopswipe==false) {
        if   (selecteditem< thumbid.length-1) {
            ++selecteditem;
            $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});
            $('.image-slider__thumb').find('input').prop('checked', false);
            $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent'});
            $('#'+thumbid[selecteditem]).css({'border' : '2px solid #826072'});
            $('#'+thumbidradio[selecteditem]).prop('checked', true);
        }
        }})

        .on('swiperight', function(e) {
          if (desktopswipe==false) {
        if (selecteditem!=0) {
          --selecteditem;
          $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});
           $('.image-slider__thumb').find('input').prop('checked', false);
            $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent'});
            $('#'+thumbid[selecteditem]).css({'border' : '2px solid #826072'});
            $('#'+thumbidradio[selecteditem]).prop('checked', true);
          }
        }
        })

        .on('movestart', function(e) {
          if ((e.distX > e.distY && e.distX < -e.distY) ||
            (e.distX < e.distY && e.distX > -e.distY)) {
          e.preventDefault();
          }
        })

        .on('move', function(e) {
          desktopswipe=true;

        var movement = 100 * e.distX / $(this).parent().width();
        $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100) + movement+'%'});
        })

        .on('moveend', function(e) {
          // if   (selecteditem< thumbid.length-1   && e.distX  < -80) {
          if   (selecteditem< thumbid.length-1   && 100 * e.distX / $(this).parent().width() < -20) {
              ++selecteditem;
              $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});

              $('.image-slider__thumb').find('input').prop('checked', false);
              $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent'});
              $('#'+thumbid[selecteditem]).css({'border' : '2px solid #826072'});
              $('#'+thumbidradio[selecteditem]).prop('checked', true);
          }
          // if (selecteditem!=0 && e.distX  > 80) {
          if (selecteditem!=0 && 100*e.distX/$(this).parent().width() > 20) {
          --selecteditem;
          $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});
           $('.image-slider__thumb').find('input').prop('checked', false);
            $('.image-slider__thumb').find('img').css({'border' : '2px solid transparent'});
            $('#'+thumbid[selecteditem]).css({'border' : '2px solid #826072'});
            $('#'+thumbidradio[selecteditem]).prop('checked', true);
          }
        else {
          $('.image-slider').find('img').first().css({'margin-left': selecteditem*(-100)+'%'});

        }
        });
  }

// create listing form
(function (app) {
  var _this;
  return _this = {
    form: null,
    subCategory: $("select", "#sub-category-box"),
    category: $("select[name=category]", this.form),
    savedAddress: $("select[name=saved-address]", this.form),
    paramBox: $("#param-box"),
    localPickUpBox: $("#local-pick-up-box"),
    checkPickUp: $("#frmCheckPickUp"),
    checkCourier: $("#frmCheckCourier"),
    rentalBox: $("#rental-box"),
    addressBox: $("#address-box"),
    confirm: $("#confirm-box"),
    useSavedAddress: $("input[name=use-saved-address]"),
    bridesmaidBox: $("#bridesmaid_list"),
    validator: null,
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, app.validator.settings, {
        rules: {
          address: "required",
          //apt: "required",
          zip: {
            zipcodeUS: true
          },
          city: {
            required: true
          }
        }
      });
      this.validator = this.form.validate(app.validator.settings);
      app.validator.init(this.form);
      app.validator.remoteZipInit(this.form);
      this.showSubCategory(0);
      var parent_category_id = ($('#parent_category_inp').val())?$('#parent_category_inp').val():'59';
      var category_id = ($('#category_inp').val())?$('#category_inp').val():'60';
      //console.log(subcat);
      this.showParam1(parent_category_id,category_id);
      this.showRentalPeriod($("input[type=checkbox]", this.rentalBox));
      this.showReason();
      this.showLocalPickUp();
      this.showAddress(!this.useSavedAddress.is(":checked"));
      this.registerHandlers();
    },
    showRentalPeriod: function (el) {
      var priceBox = el.parent().next().children(),
        price;
      //
      $.each(el, function(index, item) {
        price = $(item).parent().next().children();
        if ($(item).is(":checked")) {
          price.show();
        } else {
          price.hide();
        };
      });
    },
    showReason: function() {
      if ($("#decline").is(":checked")) {
        $("#decline_reason").show();
      }
      else {
        $("#decline_reason").hide();
      }
    },
    showSubCategory: function (id) {
      this.subCategory.hide().eq(id).show();
    },
    showParam: function (cat, subcat) {
      $("div.param", this.paramBox).hide();
      $("#param-"+cat+"-"+subcat).show();
    },
    showParam1: function (cat) {
      var subcat = '';
      var temp = 'select[name=sub-category-'+cat+']';
      subcat = $(temp).val();
      //console.log(cat);
      //console.log(subcat);
      $("div.param").hide();
      if(cat == '59' && (subcat == '60' || subcat == '78'))
        $(".length").show();
      if(cat == '59')
        $(".color, .size, .waist, .fit").show();
      if(cat == '63')
        $(".color, .shoe_size").show();
      if(cat == '67')
        $(".color").show();

      if(subcat == '74')
        $("#waist, #hip").hide();
      else
        $("#waist, #hip").show();

      if(subcat == '77' || subcat == '78')
        $("#bust").hide();
      else
        $("#bust").show();
    },
    showLocalPickUp: function () {
      this.localPickUpBox.hide();
      if (this.checkPickUp.is(":checked") || this.checkCourier.is(":checked")) {
        this.localPickUpBox.show();
      };
    },
    showAddress: function (show) {
      if (show) {
        _this.addressBox.show();
        _this.savedAddress.hide();
        return;
      };
      _this.addressBox.hide();
      _this.savedAddress.show();
    },
    registerHandlers: function () {
      this.category.on('change', function (e) {
        var cat_id = e.target.value;
        var index = e.target.selectedIndex;
        _this.showSubCategory(index);
        //var subcat = '';

        _this.showParam1(cat_id);
      });
      this.subCategory.on('change', function (e) {
        var subIndex = e.target.selectedIndex;
        var subCat = e.target.value;
        catIndex = _this.category[0].selectedIndex;
        _this.showParam1($("#top_category").val());
        //_this.showParam(catIndex, subIndex);
      });
      this.useSavedAddress.on('change', function (e) {
        _this.showAddress(!$(this).is(":checked"));
      });
      this.rentalBox.on('change', "input[type=checkbox]", function (e) {
        _this.showRentalPeriod($(this));
      });
      this.confirm.on('change', "input[type=radio]", function (e) {
        _this.showReason();
      });
      this.checkPickUp.add(this.checkCourier).on("change", function (e) {
        _this.showLocalPickUp();
      });
      this.form.find("#bridesmaid_checkbox").on("change", function() {
        _this.bridesmaidBox.slideToggle();
      });
    }
  }
})(poshareApp).init($("#create-listing"));

// profile form
(function (app) {
  var _this;
  return _this = {
    form: null,
    validator: null,
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, app.validator.settings, {
        rules: {
          zip: {
            required: false,
            zipcodeUS: true
          },
          city: {
            required: true
          }
        }
      });
      this.validator = this.form.validate(app.validator.settings);
      app.validator.init(this.form);
      app.validator.remoteZipInit(this.form);
      $('#save-image').on("click", function (e) {
        app.validator.btnCondition(_this.form);
      });
    }
  }
})(poshareApp).init($('#profile-form'));


//address form
// (function(app) {
//  var _this;
//  return _this = {
//    data: {},
//    validator: null,
//    btnArr: null,
//    el: {
//      form: null,
//      container: $('#fieldset-container'),
//      save_btn: $('#save'),
//      getFieldset: function(p) {
//        var fieldset = this.container.children("fieldset");
//        if (p == 'size') {
//          return fieldset.size();
//        };
//        return fieldset;
//      },
//      tmpl: $("#fieldset-tmpl"),
//      add_btn: $('button.add_button'),
//      getRmBtn: function() {
//        return $('button.remove_button');
//      }
//    },
//    // obj methods
//    init: function(form) {
//      // check form to init script
//      if (!form.length)
//        return;
//      // init variables
//      this.el.form = form;
//      this.btnArr = [this.el.save_btn, this.el.add_btn];
//      //
//      app.validator.buttonArr = this.btnArr;
//      app.validator.init(this.el.form);
//      app.validator.remoteZipInit(this.el.form);
//      this.addEventHandlers();
//      this.btnRemoveHandler();
//      this.validator = this.initValidator();
//      this.resetNames();
//      $.validator.addMethod("regex1", function(value, element) {
//  return this.optional(element) || /^[a-z0-9\-\s\#\/\:\,\-]+$/i.test(value);
// }, "Field must contain only letters, numbers, space and ': # / , -'");
//    },
//    initValidator: function() {
//      // $.extend(true, app.validator.settings, {message: 'hello'});
//      return this.el.form.validate(app.validator.settings);
//    },
//    addElemValidation: function(el) {
//      var name = el.data('name');
//      //
//      if (name == "street") {
//        el.rules('add', {required: true, regex1: true });
//      }
//      /*else if (name == "apt") {
//        el.rules('add', {required: true, regex1: true });
//      }*/
//      else if (name == "city") {
//        el.rules('add', {required: true, lettersonly: false});
//      }
//      else if (name == "zip") {
//        el.rules('add', {
//          required: true,
//          zipcodeUS: true,
//          messages: {
//            zipcodeUS: "Invalid ZIP"
//          }
//        });
//      };
//    },
//    addEventHandlers: function() {
//      this.el.add_btn.on('click', function (e) {
//        var cloned = _this.el.tmpl.clone().show();
//        _this.el.container.append( cloned );
//        _this.resetNames();
//        _this.btnRemoveHandler();
//        //app.validator.btnCondition(_this.el.form);
//      });
//      // remove btn handler
//      this.el.container.on('click', this.el.getRmBtn(), function (e) {
//        if (e.target.nodeName == "BUTTON") {
//          $(e.target).parents("fieldset").remove();
//          _this.resetNames();
//          _this.btnRemoveHandler();
//          //app.validator.btnCondition(_this.el.form);
//        };
//      });
//    },
//    btnRemoveHandler: function() {
//      var size = _this.el.getFieldset('size');
//      if (size > 1) {
//        _this.el.getRmBtn().show();
//      } else {
//        _this.el.getRmBtn().hide();
//      };
//    },
//    resetNames: function() {
//      var fieldset = _this.el.getFieldset();
//      fieldset.each(function(i, el) {
//        $('input, select', el).each(function(k, el) {
//          var name = $(el).data('name');
//          $(el).attr("name", name+"["+ i +"]");
//          // add validation rules
//          _this.addElemValidation($(el));
//        });
//      });
//    }
//  };
// })(poshareApp).init($('.usps_zip'));

//payout second form
(function(app) {
  var _this;
  return _this = {
    data: {},
    validator: null,
    btnArr: null,
    el: {
      form: null,
      container: $('#fieldset-container-2'),
      save_btn: $('#save'),
      getFieldset: function(p) {
        var fieldset = this.container.children("fieldset");
        if (p == 'size') {
          return fieldset.size();
        };
        return fieldset;
      },
      tmpl: $("#fieldset-tmpl"),
      add_btn: $('button.add_button'),
      getRmBtn: function() {
        return $('button.remove_button');
      }
    },
    // obj methods
    init: function(form) {
      // check form to init script
      if (!form.length)
        return;
      // init variables
      this.el.form = form;
      this.btnArr = [this.el.save_btn, this.el.add_btn];
      //
      app.validator.buttonArr = this.btnArr;
      app.validator.init(this.el.form);
      app.validator.remoteZipInit(this.el.form);
      this.addEventHandlers();
      this.btnRemoveHandler();
      this.validator = this.initValidator();
      this.resetNames();
      $.validator.addMethod("regex1", function(value, element) {
  return this.optional(element) || /^[a-z0-9\-\s\#\/\:\,\-]+$/i.test(value);
}, "Field must contain only letters, numbers, space and ': # / , -'");
    },
    initValidator: function() {
      // $.extend(true, app.validator.settings, {message: 'hello'});
      return this.el.form.validate(app.validator.settings);
    },
    addElemValidation: function(el) {
      var name = el.data('name');
      //
      if (name == "street") {
        el.rules('add', {required: true, regex1: true });
      }
      /*else if (name == "apt") {
        el.rules('add', {required: true, regex1: true });
      }*/
      else if (name == "city") {
        el.rules('add', {required: true, lettersonly: true});
      }
      else if (name == "zip") {
        el.rules('add', {
          required: true,
          zipcodeUS: true,
          messages: {
            zipcodeUS: "Invalid ZIP"
          }
        });
      };
    },
    addEventHandlers: function() {
      this.el.add_btn.on('click', function (e) {
        var cloned = _this.el.tmpl.clone().show();
        _this.el.container.append( cloned );
        _this.resetNames();
        _this.btnRemoveHandler();
        //app.validator.btnCondition(_this.el.form);
      });
      // remove btn handler
      this.el.container.on('click', this.el.getRmBtn(), function (e) {
        if (e.target.nodeName == "BUTTON") {
          $(e.target).parents("fieldset").remove();
          _this.resetNames();
          _this.btnRemoveHandler();
          //app.validator.btnCondition(_this.el.form);
        };
      });
    },
    btnRemoveHandler: function() {
      var size = _this.el.getFieldset('size');
      if (size > 1) {
        _this.el.getRmBtn().show();
      } else {
        _this.el.getRmBtn().hide();
      };
    },
    resetNames: function() {
      var fieldset = _this.el.getFieldset();
      fieldset.each(function(i, el) {
        $('input, select', el).each(function(k, el) {
          var name = $(el).data('name');
          $(el).attr("name", name+"["+ i +"]");
          // add validation rules
          _this.addElemValidation($(el));
        });
      });
    }
  };
})(poshareApp).init($('#registered-company-form'));

// registration form
(function () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      next: $('.register-next')
      ,regBtn: $('#register-btn')
      ,email: $('[name=email]', this.form)
      ,password: $('[name=password]', this.form)
      ,zip: $('[name=zip]', this.form)
      ,name: $('[name=name]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      
      $.validator.addMethod("regex", function(value, element) {
        return this.optional(element) || /^(?:[A-Z0-9]+([- ]?[A-Z0-9]+)*)?$/i.test(value);
      }, "Field must contain only letters, numbers, or dashes.");
      
      this.validator = this.form.validate({
        errorElement: 'div',
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true,
            remote: {
              url: "/index.php?route=account/register/check_email",
              type: "post"
            }
          },
          terms_agree: {
            required : true
          },
          name: {
            required: true,
            rangelength: [1, 30],
            letterswithbasicpunc: true
          },
          password: {
            required: false,
            minlength: 6
          },
          zip: {
            required: false,
            zipcodeUS: true
          }
        },
        messages: {
          zip: "Invalid Zip code",
          email: "This email is invalid or already in use",
          name: "Invalid name",
          terms_agree: "Check the checkbox",
          password: "Please enter at least 6 symbols"
        },
        submitHandler: function(form) {
           form.submit();
        }
        ,onkeyup: function (el) {
          _this.btnDisabled(el);
        }
        ,onfocusout: function (el) {
          _this.validator.element($(el));
        },
        errorPlacement: function(label, element){
          if (element.attr('name') == 'terms_agree') {
            element.parent().append(label);
          } else {
            label.insertAfter(element);
          }

          element.focus(function(){
          $(this).removeClass('error').next('div.error').hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().checkForm()) {
        _this.el.next.add(_this.el.regBtn).attr("disabled", false);
      } else {
        _this.el.next.add(_this.el.regBtn).attr("disabled", true);
      };
    },
    handlers: function () {
      $('input[type=text]', _this.form).on("change", function(e){
        _this.btnDisabled(e.currentTarget);
      });
      showpassword($('#show-pass--register'),$('#password--register'))
      _this.el.next.click(function(){
        // console.log(_this.form.validate().checkForm())
        if (!_this.validator.element(_this.el.email)) {
          return false;
        };
        // _this.el.regBtn.attr("disabled", true);
      });
    }
  }
})().init($('#registration-form'));

// reset password form
(function (mainApp) {
var _this;
  return _this = {
    validator: null,
    form: null,
    app: $.extend({}, mainApp),
    el: {
      regBtn: $('#reset-btn'),
      email: $('[name=reset__email]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, this.app.validator.settings, {
        rules: {
          reset__email: {
            required: false,
            email: true
          }
        }
      });
      this.validator = this.form.validate(this.app.validator.settings);
      this.app.validator.buttonArr = this.el.regBtn;
      this.app.validator.init(this.form);
      this.handlers();
    },
    handlers: function () {
      $('.login-button').click(function(){
        $('#login-form').css('display','block');
        $('#password-form').css('display','none');
        $('#login-form .login__header').text('Log In');
      });
    }
  }
})(poshareApp).init($('#password-form'));

//login form
(function (mainApp) {
var _this;
  return _this = {
    validator: null,
    form: null,
    app: $.extend({}, mainApp),
    el: {
      regBtn: $('#login-btn'),
      email: $('[name=loginemail]', this.form),
      password: $('[name=loginpassword]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, this.app.validator.settings, {
        rules: {
          loginemail: {
            required: false,
            email: true
          },
          loginpassword: {
            required: false,
            minlength: 6
          }
        }
      });
      this.validator = this.form.validate(this.app.validator.settings);
      this.app.validator.buttonArr = this.el.regBtn;
      this.app.validator.init(this.form);
      this.handlers();
    },
    handlers: function () {
      showpassword($('#show-pass--login'),$('#login--password'));
      $('.forgot-pass').click(function(){
        // console.log('you forgot the pass');
        $('#login-form').css('display','none');
        $('#password-form').css('display','block');
        $('#login-form .login__header').text('Reset Password');
      });
    }
  }
})(poshareApp).init($('#login-form'));

// fixed registration form
(function () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      next: $('.register-next')
      ,regBtn: $('#register-btn--fixed')
      ,email: $('[name=email]', this.form)
      ,password: $('[name=password]', this.form)
      ,zip: $('[name=zip]', this.form)
      ,name: $('[name=name]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      
      $.validator.addMethod("regex", function(value, element) {
        return this.optional(element) || /^(?:[A-Z0-9]+([- ]?[A-Z0-9]+)*)?$/i.test(value);
      }, "Field must contain only letters, numbers, or dashes.");
      
      this.validator = this.form.validate({
        errorElement: 'div',
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true,
            remote: {
              url: "/index.php?route=account/register/check_email",
              type: "post"
            }
          },
          terms_agree: {
            required : true
          },
          name: {
            required: true,
            rangelength: [1, 30],
            letterswithbasicpunc: true
          },
          password: {
            required: false,
            minlength: 6
          },
          zip: {
            required: false,
            zipcodeUS: true
          }
        },
        messages: {
          zip: "Invalid Zip code",
          email: "This email is invalid or already in use",
          name: "Invalid name",
          terms_agree: "Check the checkbox",
          password: "Please enter at least 6 symbols"
        },
        submitHandler: function(form) {
           form.submit();
        }
        ,onkeyup: function (el) {
          _this.btnDisabled(el);
        }
        ,onfocusout: function (el) {
          _this.validator.element($(el));
        },
        errorPlacement: function(label, element){
          if (element.attr('name') == 'terms_agree') {
            element.parent().append(label);
          } else {
            label.insertAfter(element);
          }

          element.focus(function(){
          $(this).removeClass('error').next('div.error').hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().checkForm()) {
        _this.el.next.add(_this.el.regBtn).attr("disabled", false);
      } else {
        _this.el.next.add(_this.el.regBtn).attr("disabled", true);
      };
    },
    handlers: function () {
      $('input[type=text]', _this.form).on("change", function(e){
        _this.btnDisabled(e.currentTarget);
      });
      showpassword($('#show-pass--register--fixed'),$('#password--register--fixed'))
      _this.el.next.click(function(){
        // console.log(_this.form.validate().checkForm())
        if (!_this.validator.element(_this.el.email)) {
          return false;
        };
        // _this.el.regBtn.attr("disabled", true);
      });
    }
  }
})().init($('#registration-form--fixed'));

// fixed reset password form
(function (mainApp) {
var _this;
  return _this = {
    validator: null,
    form: null,
    app: $.extend({}, mainApp),
    el: {
      regBtn: $('#reset-btn--fixed'),
      email: $('[name=reset__email]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, this.app.validator.settings, {
        rules: {
          reset__email: {
            required: false,
            email: true
          }
        }
      });
      this.validator = this.form.validate(this.app.validator.settings);
      this.app.validator.buttonArr = this.el.regBtn;
      this.app.validator.init(this.form);
      this.handlers();
    },
    handlers: function () {
      $('.login-button').click(function(){
        $('#login-form--fixed').css('display','block');
        $('#password-form--fixed').css('display','none');
        $('#login-form--fixed .login__header').text('Log In');
      });
    }
  }
})(poshareApp).init($('#password-form--fixed'));

// fixed login form
(function (mainApp) {
var _this;
  return _this = {
    validator: null,
    form: null,
    app: $.extend({}, mainApp),
    el: {
      regBtn: $('#login-btn--fixed'),
      email: $('[name=loginemail]', this.form),
      password: $('[name=loginpassword]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;
      //
      this.form = form;
      $.extend(true, this.app.validator.settings, {
        rules: {
          loginemail: {
            required: false,
            email: true
          },
          loginpassword: {
            required: false,
            minlength: 6
          }
        }
      });
      this.validator = this.form.validate(this.app.validator.settings);
      this.app.validator.buttonArr = this.el.regBtn;
      this.app.validator.init(this.form);
      this.handlers();
    },
    handlers: function () {
      showpassword($('#show-pass--login--fixed'),$('#login--password--fixed'));
      $('.forgot-pass').click(function(){
        // console.log('you forgot the pass');
        $('#login-form--fixed').css('display','none');
        $('#password-form--fixed').css('display','block');
        $('#login-form--fixed .login__header').text('Reset Password');
      });
    }
  }
})(poshareApp).init($('#login-form--fixed'));


//support form
function supportFormValidate () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      regBtn: $('#support-btn'),
      email: $('[name=email]', this.form),
      name: $('[name=name]', this.form),
      phone: $('[name=phone]', this.form),
      message: $('[name=message]', this.form),
      reason: $('[name=reason]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;

      this.form = form;
      this.validator = this.form.validate({
        errorElement: 'div',
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true
          },
          name: {
            required: true,
            rangelength: [1, 15]
          },
          phone: {
            required: true
          },
          message: {
            required: true,
            minlength: 10
          },
          reason: {
            required: true,
            reason: true
          }
        },
        messages: {
          phone: "Invalid phone number",
          email: "Invalid email",
          name: "Invalid name",
          message: "Please enter at least 10 symbols",
          reason: "Please select reason"
        },
        submitHandler: function(form) {
          if (form.valid()) {
            form.submit();
          }
        },
        errorPlacement: function(label, element){
          label.insertAfter(element);
          element.focus(function(){
          $(this).removeClass('error').next('div.error').hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      $.validator.addMethod("reason", function(value, element) {
        return $(element).find(":selected").index() != 0;
      });
      this.validator.resetForm();
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().valid()) {
        _this.el.regBtn.attr("disabled", false);
      } else {
        _this.el.regBtn.attr("disabled", true);
      };
    },
    handlers: function () {
      $('input[type=text], textarea, select', _this.form).on("change", function(e){
        _this.validator.element($(this));
      });
    }
  }
}

//problem with order form
(function () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      regBtn: $('#orderProblem-btn'),
      email: $('[name=email]', this.form),
      name: $('[name=name]', this.form),
      phone: $('[name=phone]', this.form),
      message: $('[name=message]', this.form),
      reason: $('[name=reason]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;

      this.form = form;
      this.validator = this.form.validate({
        errorElement: 'div',
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true
          },
          name: {
            required: true,
            rangelength: [1, 15]
          },
          phone: {
            required: false,
            minlength: 6
          },
          message: {
            required: true,
            minlength: 10
          },
          reason: {
            required: true,
            reason: true
          }
        },
        messages: {
          phone: "Invalid phone number",
          email: "Invalid email",
          name: "Invalid name",
          message: "Please enter at least 10 symbols",
          reason: "Please select reason"
        },
        submitHandler: function(form) {
          if (form.valid()) {
            form.submit();
          }
        },
        errorPlacement: function(label, element){
          label.insertAfter(element);
          element.focus(function(){
          $(this).removeClass('error').next('div.error').hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      $.validator.addMethod("reason", function(value, element) {
        return $(element).find(":selected").index() != 0;
      });
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().valid()) {
        _this.el.regBtn.attr("disabled", false);
      } else {
        _this.el.regBtn.attr("disabled", true);
      };
    },
    handlers: function () {
      $('input[type=text], textarea, select', _this.form).on("change", function(e){
        _this.validator.element($(this));
      });
    }
  }
})().init($('#orderProblem-form'));

//contact us form
(function () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      regBtn: $('#contact--btn'),
      email: $('[name=email]', this.form),
      name: $('[name=name]', this.form),
      subject: $('[name=subject]', this.form),
      message: $('[name=message]', this.form)
    },
    init: function (form) {
      if (!form.length)
        return;

      this.form = form;
      this.validator = this.form.validate({
        errorElement: 'div',
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true,
            remote: {
              url: "/index.php?route=account/register/check_email",
              type: "post"
            }
          },
          name: {
            required: true,
            rangelength: [1, 15]
          },
          message: {
            required: true,
            minlength: 10
          }
        },
        messages: {
          email: "Invalid email",
          name: "Invalid name",
          message: "Please enter at least 10 symbols"
        },
        submitHandler: function(form) {
          if (form.valid()) {
            form.submit();
          }
        },
        errorPlacement: function(label, element) {
          label.insertBefore(element);
          element.focus(function() {
            $(this).removeClass('error').next('div.error').hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().valid()) {
        _this.el.regBtn.attr("disabled", false);
      } else {
        _this.el.regBtn.attr("disabled", true);
      };
    },
    handlers: function () {
      $('input[type=text], textarea', _this.form).on("change", function(e){
        _this.validator.element($(this));
      });
    }
  }
})().init($('#contact-form'));


//signUp form
(function () {
var _this;
  return _this = {
    validator: null,
    form: null,
    el: {
      regBtn: $("#signUp2--button"),
      email: $("#signUp2--email", this.form)
    },
    init: function (form) {
      if (!form.length)
        return;

      this.form = form;
      this.validator = this.form.validate({
        errorElement: "div",
        focusInvalid: false,
        rules: {
          email: {
            required: true,
            email: true
          }
        },
        messages: {
          email: "This email is invalid or already in use"
        },
        submitHandler: function(form) {
          _this.form.slideUp(0);
          _this.form.parent().find(".signUp__info").fadeIn(200);
          return false;
        },
        errorPlacement: function(label, element) {
          label.insertAfter(element);
          element.focus(function() {
            $(this).removeClass("error").next("div.error").hide();
          });
        }
      });
      $.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(value);
      }, "Invalid email");
      this.handlers();
    },
    btnDisabled: function (el) {
      if (_this.form.validate().valid()) {
        _this.el.regBtn.attr("disabled", false);
      } else {
        _this.el.regBtn.attr("disabled", true);
      };
    },
    handlers: function () {
      $("input[type=text]", _this.form).on("change", function(e){
        _this.validator.element($(this));
      });
    }
  }
})().init($("#signUp2"));



// *****




  // function bagscroll() {
  //   var lastScrollTop = 0;
  //   $(window).scroll(function(event) {

  //       if ($(window).width() > 719) {
  //       var st = $(this).scrollTop();


  //       var elpostop = $('.bag__summary').position().top-10;

  //       var elheight = $('.bag__summary').innerHeight()+10;


  //       var elheightmurgin = $('.bag__summary').outerHeight(true)+10;

  //       var winscrollpos = $(window).scrollTop();

  //       var winheight = $(window).height();


  //       if (st > lastScrollTop) {
  //         // Scroll down function


  //         if ($('.content').height() - elheightmurgin != 0 && $('.content').height() - elheightmurgin > 0 ) { // Check if block has not reached parent's bottom

  //           //if (elpostop < winscrollpos) { // Run the script if scrolled window reaches element's parent
  //           if (elheight + elpostop < $('.content').height()) {

  //             if ($('.bag__summary').height() > winheight) { // Check if element is larger than window

  //               var elementbuttom = elheightmurgin + elpostop - winheight;

  //               if (winscrollpos > elementbuttom) {
  //                 $('.bag__summary').css('top',  elheightmurgin - elheight + winscrollpos - elementbuttom); //so scroll from bottom of the element
  //               }
  //             }
  //             else {
  //               $('.bag__summary').css('top', winscrollpos - elpostop); //else scroll from top

  //             }

  //           } else {
  //             $('.bag__summary').css('top', 0); // Get element back if scrolled window leaves element's parent
  //           }
  //         }
  //       } else {
  //         // Scroll Up
  //          if (winscrollpos < elpostop + elheightmurgin - elheight) {
  //           if ((winscrollpos - elpostop) > 0) {
  //             $('.bag__summary').css('top', winscrollpos - elpostop);
  //           } else {
  //             $('.bag__summary').css('top', 0);
  //           }
  //         }
  //       }

  //       lastScrollTop = st;
  //     } else {
  //       $('.bag__summary').css('top', 0);
  //      return;
  //      }

  //   });

  // }

  function bagscroll() {
    var lastScrollTop = 0;
    $(window).scroll(function(event) {

        if ($(window).width() > 719) {

        var scrollTop = $(this).scrollTop(),
            $content = $('.content'),
            $bag = $('.bag__summary'),
            bagHeight = $bag.innerHeight(),
            topPos = $bag.parent().position().top,
            movableArea = $content.innerHeight() - topPos;

        if (scrollTop >= $content.offset().top + movableArea / 2) {
          if ( $bag.outerHeight() + (scrollTop - $content.offset().top - movableArea / 2) <= movableArea ) {
            $bag.css('top', scrollTop - $content.offset().top - movableArea / 2);
          }
        } else {
          $bag.css('top', 0);
        }

      } else {
        $('.bag__summary').css('top', 0);
       return;
       }

    });

  }







  function image_crop (displaybutton, cropblock, profileFlag) {
    var profileFlag = profileFlag || 0,
        image = displaybutton.data("url"),
        cropblockImage = cropblock.find('.cropit-image-preview > img'),
        minImgHeight = profileFlag ? 200 : 900,
        minImgWidth = profileFlag ? 200 : 603;
    cropblockImage.cropper({
      aspectRadio: 2 / 3,
      autoCropArea: 1,
      strict: false,
      guides: false,
      center: false,
      highlight: false,
      dragCrop: false,
      cropBoxMovable: false,
      cropBoxResizable: false,
      minCropBoxHeight: minImgHeight,
      minCropBoxWidth: minImgWidth,
      built: function() {
        var zoomValue = 300 / $(this).cropper('getCanvasData').height - 1;
        $(this).cropper('zoom', zoomValue);
      }
    });

    displaybutton.click(function(e) {
      if (e.target.tagName == 'A') {
        var originalName = $(this).find('input[type="hidden"]')[0].val(),
                cropName = $(this).find('input[type="hidden"]')[1].val();
        $.ajax({
          url: '/index.php?route=product/product/delImage',
          type: 'POST',
          data: 'original=' + originalName + '&crop=' + cropName,
          dataType: 'json',
          success: function(json) {
            displaybutton.attr("data-url", "img/add.png");
            displaybutton.find("img").attr("src", "img/add.png");
            $('.export').unbind();
            displaybutton.find('.grid_photo_delete').remove();
            displaybutton.find('input[name*="original"]').val('');
            displaybutton.find('input[name*="crop"]').val('');
          }
        });
      } else {
        centerblock($('.image-editor'), 'fixed');
        image = displaybutton.attr("data-url");
        id = displaybutton.attr("id");

        cropblockImage.cropper('replace',image);
        cropblock.addClass('show');
        $('.lightbox').addClass('show');

        $('#rotate').click(function(){
          cropblockImage.cropper('rotate', 90);
        });

        $('#rotate-ccw').click(function(){
          cropblockImage.cropper('rotate', -90);
        });

        $('#zoom-in').click(function() {
          cropblockImage.cropper('zoom', 0.01);
        });

        $('#zoom-out').click(function() {
          cropblockImage.cropper('zoom', -0.01);
        });

        $('#image_uploader').change(function() {
          var reader = new FileReader();
          reader.onload = function(e) {
            cropblockImage.attr('src', e.target.result);
            cropblockImage.cropper('replace', cropblockImage.attr('src'));
          }
          reader.readAsDataURL( this.files[0] );
        });

        $('#cancel').click(function(){
          cropblock.removeClass('show');
          $('.lightbox').removeClass('show');
          $('.export').unbind();
          $('#rotate').unbind();
          $('#rotate-ccw').unbind();
          $('#rotate-in').unbind();
          $('#rotate-out').unbind();
          $('#image_uploader').unbind();
          $('#image_uploader').val('');
        });


        $('.export').click(function() {
          cropblock.removeClass('show');
          $('.lightbox').removeClass('show');
          displaybutton.attr("data-url", cropblockImage.attr('src'));
          cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toBlob(function (blob) {
            displaybutton.find('img').attr("src", URL.createObjectURL(blob));
            console.log(URL.createObjectURL(blob)); 
          });

          $('#image_uploader').val('');

          if(id == 'crop1') {
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              data: 'original1=' + displaybutton.attr('data-url') + 
                    '&crop1=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              success: function(json) {
                $('input[name=original1]').attr('value',json['original']);
                $('input[name=crop1]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          } else if(id == 'crop_profile'){
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              data: 'original_profile=' + displaybutton.attr('data-url') + 
                    '&crop_profile=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              success: function(json) {
                $('input[name=original_profile]').attr('value',json['original']);
                $('input[name=crop_profile]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          } else if(id == 'crop2'){
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              data: 'original2=' + displaybutton.attr('data-url') + 
                    '&crop2=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              success: function(json) {
                $('input[name=original2]').attr('value',json['original']);
                $('input[name=crop2]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          } else if(id == 'crop3'){
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              data: 'original3=' + displaybutton.attr('data-url') + 
                    '&crop3=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              success: function(json) {
                $('input[name=original3]').attr('value',json['original']);
                $('input[name=crop3]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          } else if(id == 'crop4'){
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              data: 'original4=' + displaybutton.attr('data-url') + 
                    '&crop4=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              success: function(json) {
                $('input[name=original4]').attr('value',json['original']);
                $('input[name=crop4]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          } else if(id == 'crop5'){
            $.ajax({
              url: '/index.php?route=product/product/saveImage',
              type: 'post',
              data: 'original5=' + displaybutton.attr('data-url') + 
                    '&crop5=' + cropblockImage.cropper('getCroppedCanvas', { width: minImgWidth, height: minImgHeight }).toDataURL(),
              dataType: 'json',
              beforeSend: function() {
                displaybutton.append('<div class="loader"></div>');
              },
              success: function(json) {
                $('input[name=original5]').attr('value',json['original']);
                $('input[name=crop5]').attr('value',json['crop']);
                displaybutton.find('.loader').remove();
              }
            });
          }

          displaybutton.append('<a class="grid_photo_delete"></a>');
          $('.export').unbind();
        });
      }
    });
  }




  function showpassword(control, block) {
    function _show() {
       if (control.is(':checked')) {
        block.prop('type', 'text');
      } else {
        block.prop('type', 'password');
      }

    }
    _show();
    control.change(function(){_show()});
  }


  function register_lightbox() {

    $('.register-button').click(function(){
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#register-step1').removeClass('show');
      centerblock($('#register-step1'));
      $('.lightbox, #register-step1').addClass('show');

      closeonclickoutside($('#register-step1'), $('.register-button'), function() {
        $('.lightbox, #register-step1').removeClass('show');
      });
    });

    $('.login-button').click(function(){
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#login').removeClass('show');
      $('#password-form').css('display', 'none');
      centerblock($('#login'));
      $('#login .login__header').text('Log In');
      $('.lightbox, #login').addClass('show');
      closeonclickoutside($('#login'), $('.login-button'), function() {
        $('.lightbox, #login').removeClass('show');
      });
    });

    $('.register-button--fixed').click(function(){
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#register-step1--fixed').removeClass('show');
      centerblock($('#register-step1--fixed'));
      $('.lightbox, #register-step1--fixed').addClass('show');

      closeonclickoutside($('#register-step1--fixed'), $('.register-button--fixed, .lightbox'), function() {
        $('.lightbox, #register-step1--fixed').removeClass('show');
      }, true);
    });

    $('.login-button--fixed').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#login--fixed').removeClass('show');
      $('#password-form--fixed').css('display', 'none');
      centerblock($('#login--fixed'));
      $('#login--fixed .login__header').text('Log In');
      $('.lightbox, #login--fixed').addClass('show');
      closeonclickoutside($('#login--fixed'), $('.login-button--fixed, .lightbox'), function() {
        $('.lightbox, #login--fixed').removeClass('show');
      }, true);
    });

    $('.contact-button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#contact_support').removeClass('show');
      centerblock($('#contact_support'));
      $('.support__header').text( $(this).text() );
      if ($(this).hasClass('dispute-button')) {
        var dropdown = document.createElement('select');
        dropdown.setAttribute('id', 'support--reason');
        dropdown.setAttribute('name', 'reason');
        var options = [{
          attr: {
            selected: 'selected',
            disabled: 'disabled'
          },
          content: 'Select Reason'
        },
        {
          attr: {
            value: 'Didn’t fit'
          },
          content: 'Didn’t fit'
        },
        {
          attr: {
            value: 'Not as described'
          },
          content: 'Not as described'
        },
        {
          attr: {
            value: 'Condition not as described'
          },
          content: 'Condition not as described'
        },
        {
          attr: {
            value: 'Not delivered on time'
          },
          content: 'Not delivered on time'
        }];
        options.forEach(function(el) {
          var option = document.createElement('option');
          for (var prop in el.attr) {
            option.setAttribute(prop, el.attr[prop]);
          }
          option.innerHTML = el.content;
          dropdown.appendChild(option);
        });
        $(dropdown).insertBefore('#support--message');
      }
      document.getElementById("support-form").reset();
      supportFormValidate().init($('#support-form'));
      $('.lightbox, #contact_support').addClass('show');
      $('.lightbox').click(function() {
        $('.lightbox, #contact_support').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
        if ($('#support--reason').length) {
          $('#support--reason').remove();
          $('#support--reason-error').remove();
        }
      });
      closeonclickoutside($('#contact_support'), $(this), function() {
        $('.lightbox, #contact_support').removeClass('show');
      });
    });

    $('.confirm-button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#confirm').removeClass('show');
      centerblock($('#confirm'));
      $('.confirm__link').attr('href', $(this).data("delete-href"));
      $('.lightbox, #confirm').addClass('show');
      $('.lightbox, .confirm__cancel').click(function() {
        $('.lightbox, #confirm').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#confirm'), function() {
        $('.lightbox, #confirm').removeClass('show');
      });
    });

    $('.track-num__button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#trackingNum').removeClass('show');
      centerblock($('#trackingNum'));
      $('.lightbox, #trackingNum').addClass('show');
      $('.lightbox, .tracking-modal__input button').click(function() {
        $('.lightbox, #trackingNum').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#trackingNum'), $(this), function() {
        $('.lightbox, #trackingNum').removeClass('show');
      });
    });

    $('.review-lender__show').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#reviewLender').removeClass('show');
      centerblock($('#reviewLender'));

      var productId = $(this).data('productId');
      var sellerId = $(this).data('sellerId');
      $('#reviewLender').find('input[name="product_id"]').val(productId);
      $('#reviewLender').find('input[name="seller_id"]').val(sellerId);

      $('.lightbox, #reviewLender').addClass('show');
      $('.lightbox').click(function() {
        $('.lightbox, #reviewLender').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#reviewLender'), $(this), function() {
        $('.lightbox, #reviewLender').removeClass('show');
      });
    });

    $('.returning-popup__button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#returningPopup').removeClass('show');
      centerblock($('#returningPopup'));
      $('.lightbox, #returningPopup').addClass('show');
      $('.lightbox, #returningPopup .button').click(function() {
        $('.lightbox, #returningPopup').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#returningPopup'), $(this), function() {
        $('.lightbox, #returningPopup').removeClass('show');
      });
    });

    $('.payout-missing__button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#payoutMissing').removeClass('show');
      centerblock($('#payoutMissing'));
      $('.lightbox, #payoutMissing').addClass('show');
      $('.lightbox, .payout-missing__cancel').click(function() {
        $('.lightbox, #payoutMissing').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#payoutMissing'), $(this), function() {
        $('.lightbox, #payoutMissing').removeClass('show');
      });
    });

    $('.problem-button').click(function() {
      if ( $(window).width() <= 720 ) {
        $('.menu__float').css('left','-250px');
        $('html').removeAttr('style');
      }
      $(window).unbind("click touchstart");
      $(document).unbind("keyup");
      $('.modal').not('#orderProblem').removeClass('show');
      centerblock($('#orderProblem'));
      $('.lightbox, #orderProblem').addClass('show');
      $('.lightbox').click(function() {
        $('.lightbox, #orderProblem').removeClass('show');
        $(window).unbind("click touchstart");
        $(document).unbind("keyup");
      });
      closeonclickoutside($('#orderProblem'), $(this), function() {
        $('.lightbox, #orderProblem').removeClass('show');
      });
    });

  }




  if ($('.order-confirm').length) {
    $('.order-confirm').each(function(el) {
      orderConfirm( $(this) );
    });
  }


  if ($('.register--absolute').length) {
    register_lightbox();
  }

  if($('.menu__nav').length) {
    menu_click_hover('.main-nav');
    menu_click_hover('.menu__dropdown');

    var ww=$(window).width(); //get current window width
    $(window).resize(function(){
      if (ww<720) {
        if ($(window).width()>=720) { //if window width passes breakpoint
          ww=$(window).width(); //change active window width
          menu_click_hover('.main-nav');
          menu_click_hover('.menu__dropdown');
        }
      }
      if (ww>=720) {
        if ($(window).width()<720) { //same as above
          ww=$(window).width();
          menu_click_hover('.main-nav');
          menu_click_hover('.menu__dropdown');
        }
      }
    });
  }

  if($('.top-menu__menu-icon').length) {
    side_menu($('.top-menu__menu-icon'), '.menu__float', 'left');
  }

  // if ($('.filter-button').length) {
  //  side_menu($('.filter-button'),$('.filter'),'right');
  // }

  if ($('.page-menu').length) {
    if ($(this).find('li').hasClass('active')==true) {
      page_menu_scroll();
    }
  }

  if ($('.image-slider')) {
    img_slider();
  }

  if ($('.bag__summary').length) {
    if ($(window).width()>=720) {
      bagscroll();
    }

    $(window).resize(function(){
      if ($(window).width()>=720) {
        bagscroll();
      }
    });
  }



  if ($('.settings_image').length) {
    image_crop($('.settings_image'),$('.image-editor'), 1);
    // centerblock($('.image-editor'));
  }




  if ($('#crop1').length) {
    image_crop($('#crop1'),$('.image-editor'), 0);
    image_crop($('#crop2'),$('.image-editor'), 0);
    image_crop($('#crop3'),$('.image-editor'), 0);
    image_crop($('#crop4'),$('.image-editor'), 0);
    image_crop($('#crop5'),$('.image-editor'), 0);
  }

  if ($('select[multiple]').length)
    $('select[multiple]').multipleSelect();


  $('.info-panel__close').on('click', function() {
    $(this).parent().slideUp();
  });


});


function centerblock(block, position) {
  function howtoposition(position) {
    var wh = $(window).height(),
        ww = $(window).width(),
        bh = block.outerHeight(),
        position = position || 'absolute',
        who = position == 'absolute' ? window.pageYOffset : 0;

    block.css('position', position);

    if (bh < wh && ww >= 450) {
      block.css({
                'margin-left':-block.outerWidth()/2,
                'margin-top':-block.outerHeight()/2,
                'top': wh/2 + who
                });
    } else if (bh > wh && ww >= 450) {
        block.css({
          'margin-left':-block.outerWidth()/2,
          'top':0,
          'margin-top': who
        });
      } else if (bh > wh && ww < 450) {
        block.css({
          'margin-left':'0',
          'margin-top':'0',
          'top': who
        });
      } else {
        block.css({
          'margin-left':'0',
          'margin-top':-block.outerHeight()/2,
          'top': wh/2 + who
        });
      }
  }

  howtoposition(position);

  $(window).resize(function(){
    howtoposition(position);
  });
}

function closeonclickoutside(box, button, func, fixed) {
  var $win = $(window);
  var $box = box;
  var $button = button;
  var fixed = fixed || false;
  $win.on("click touchstart", function(event) {
    if ($box.has(event.target).length == 0 && !$box.is(event.target) && !$button.is(event.target) && $button.has(event.target).length == 0) {
      func();
      $win.unbind("click touchstart");
      $(document).unbind("keyup");
    }
  });
  if (!fixed) {
    $(document).keyup(function(e) {
      if (e.keyCode == 27) {
        func();
        $win.unbind("click touchstart");
        $(document).unbind("keyup");
      }
    });
  }
}

// Toggle for new data
function toggleNewData(checkbox) {
  if (!checkbox) return;

  if ( $(checkbox).is(':not(:checked)') ) {
    $(checkbox).parents('fieldset').find('.new_data').slideDown(200);
  } else {
    $(checkbox).parents('fieldset').find('.new_data').slideUp(200);
  }

  $(checkbox).on('click', function() {
    if ( $(this).is(':not(:checked)') ) {
      $(this).parents('fieldset').find('.new_data').slideDown(200);
    } else {
      $(this).parents('fieldset').find('.new_data').slideUp(200);
    }
  });
}

function side_menu(button, block, side) { //display block as a side menu
  var sideisopen=false,
      closedonclick,
      otherside;


  if (side == 'left') {
    otherside ='right';
  } else if (side == 'right') {
    otherside = 'left';
  }

  button.off('click');
  button.on('click', function() {
    if ($(block).css(side) != '0px') {
      closedonclick=false;
      sideisopen=true;

      $('html').css('position','fixed');
      $('html').css(side,'250px');
      $('html').css(otherside,'-250px');

      $(block).css(side,'0px');

      $(function() {
        var $win = $(window);
        var $button = button;

        $win.on("click touchstart", function(event) {
          if (sideisopen==true && closedonclick!=true) {
            var $box = $(block);
            if ($box.has(event.target).length == 0 && !$box.is(event.target) && $button.has(event.target).length == 0 && !$button.is(event.target)) {
              event.preventDefault();
              $(block).css(side,'-250px');
              $('html').removeAttr('style');
              sideisopen=false;
            }
          }
        });
      });
    }
    else {
      $(block).css(side,'-250px');
      $('html').removeAttr('style');
      sideisopen=false;
      closedonclick=true;
    }
  });

  $(window).resize(function(){
    if ($(window).width()>=720) {
      $('html').removeAttr('style');
      sideisopen=false;
    } else {
        if (sideisopen!=true) {
          $(block).css(side,'-250px');
        }
    }
  });
}

// Order confirm

function orderConfirm(block) {
  var $block = block,
      $confirm = $block.find('.order-confirm__confirm'),
      $info = $block.find('.order-confirm__info'),
      $trackInput = $block.find('.track-num__input'),
      $trackSubmit = $trackInput.find('button'),
      $selectItems = $block.find('.card-select__items');

  $confirm.on('click', function() {
    $block.find('.shipping-soon').trigger('click');
    $(this).addClass('disabled');
  });

  $selectItems.find('a').on('click', function() {
    $info.find('.card-select__title').text( $(this).text() );
    if ( $(this).hasClass('shipping-soon') ) {
      $info.addClass('button--lightred');
    }
    else {
      $info.removeClass('button--lightred');
    }
  });

  $trackSubmit.on('click', function() {
    $(this).fadeOut(200);
  });

  $trackInput.find('input').on('click', function() {
    $trackSubmit.fadeIn(200);
  });

}

//    Colors Filter

function colorsFilter(colorsNum) {
  var maxNum = colorsNum || 1,
      colors = $('.colors-view a'),
      colorsArr = [];

  $('.colors-view a.active').each(function() {
    colorsArr.push(this);
  });

  colors.on('click', function() {

    if ( colorsArr.indexOf(this) != -1 ) {
      colorsArr.splice(colorsArr.indexOf($(this)), 1);
      $(this).removeClass('active');
    } else {
      if ( colorsArr.length >= maxNum )
        $(colorsArr.shift()).removeClass('active');
      $(this).addClass('active');
      colorsArr.push(this);
    }
    
  });
};

// Polyfill for HTMLCanvasElement.toBlob()

if (!HTMLCanvasElement.prototype.toBlob) {
 Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
  value: function (callback, type, quality) {

    var binStr = atob( this.toDataURL(type, quality).split(',')[1] ),
        len = binStr.length,
        arr = new Uint8Array(len);

    for (var i=0; i<len; i++ ) {
     arr[i] = binStr.charCodeAt(i);
    }

    callback( new Blob( [arr], {type: type || 'image/png'} ) );
  }
 });
}
